<?php

$plugin = array(
  'title' => t('Flat Rate'),
  'description' => t('A flat rate shipping method.'),
  'handler' => array(
    'class' => 'shipping_flat_rate',
  ),
);
